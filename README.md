# RSFC Boundaries Wig NI 2014

This package contains resting-state functional correlation (RSFC) generated boundaries and snowballing peak density map based on 120 individuals. Seen in publication by [Wig et. al 2014 (NeuroImage)](https://linkinghub.elsevier.com/retrieve/pii/S105381191300791X).



## RSFC boundary mapping 

<img src="img/boundary_32k_L_lateral.png" alt="RSFC Boundaries" width="400" height="281">


## RSFC snowballing

<img src="img/snowballing_32k_L_lateral.png" alt="RSFC Snowballing" width="400" height="281">

__Reference:__

Wig, G. S., Laumann, T. O., Petersen, S. E. (2014). An approach for parcellating human cortical areas using resting-state correlations. NeuroImage, 93(2), 276-291. https://doi.org/10.1016/j.neuroimage.2013.07.035

Wig, G. S., Laumann, T. O., Cohen, A. L., Power, J. D., Nelson, S. M., Glasser, M. F., Miezin, F. M., Snyder, A. Z., Schlaggar, B. L., & Petersen, S. E. (2014). Parcellating an individual subject's cortical and subcortical brain structures using snowball sampling of resting-state correlations. Cerebral Cortex, 24(8), 2036–2054. https://doi.org/10.1093/cercor/bht056